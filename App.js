import React from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import Game from "./src/screens/Game";

const {height} = Dimensions.get('window');

const App = () => (
  <View style={styles.root}>
    <Game />
  </View>
);

const styles = StyleSheet.create({
  root: {
    backgroundColor: 'white',
    padding: "10px",
    height: height,
  },
});

export default App;
