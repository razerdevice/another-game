import React from 'react';
import {Text} from 'react-native';

const MyText = ({children, style, ...restProps}) => {
  return (
    <Text style={[{fontFamily: 'Helvetica Neue'}, style]} {...restProps}>
      {children}
    </Text>
  );
};

export default MyText;
