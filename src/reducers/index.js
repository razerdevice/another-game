import {combineReducers} from 'redux';
import mainReducer from './mainReducer';
import cameraRollReducer from './cameraRollReducer';

const appReducer = combineReducers({
  mainReducer,
  cameraRollReducer,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
