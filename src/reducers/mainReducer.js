import ACTION from '../actions/actionTypes';
import {intitialGrid} from '../grid/GridUtils';

const initialState = {
  appName: 'Poccket',
  grid: intitialGrid,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ACTION.TEST_ACTION: {
      return {
        ...state,
        appName: action.data,
      };
    }
    case ACTION.UPDATE_GRID: {
      return {
        ...state,
        grid: action.data,
      };
    }
    default: {
      return state;
    }
  }
}
