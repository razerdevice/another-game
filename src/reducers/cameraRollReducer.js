import ACTION from '../actions/actionTypes';

const initialState = {
  photos: [],
  selected: [],
  endCursor: null,
  hasNext: true,
  isFetching: false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ACTION.SET_PHOTOS: {
      return {
        ...state,
        ...action.data,
      };
    }
    case ACTION.SET_FETCHING: {
      return {
        ...state,
        isFetching: action.data,
      };
    }
    case ACTION.SET_SELECTED_PHOTOS: {
      return {
        ...state,
        selected: action.data,
      };
    }
    default: {
      return state;
    }
  }
}
