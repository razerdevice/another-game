import {PermissionsAndroid} from 'react-native';
import {Dimensions, Platform} from 'react-native';
const {height} = Dimensions.get('window');

export const modArray = (array, value) => {
  const index = array.indexOf(value);

  if (!~index) {
    array.push(value);
  } else {
    array.splice(index, 1);
  }
  return [...array];
};

export const isLast = (index, numColumns) => (index + 1) % numColumns === 0;

export const permissionAndroid = async () => {
  try {
    return await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      {
        title: 'App Gallery Permission',
        message: 'App needs access to your gallery ',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
  } catch (e) {
    console.log(e);
  }
};

export const isIphoneX = () =>
  Platform.OS === 'ios' && (isIPhoneXSize() || isIPhoneXrSize());

export const isIPhoneXSize = () => height === 812;

export const isIPhoneXrSize = () => height === 896;
