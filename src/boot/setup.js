import React from 'react';
import configureStore from './configureStore';
import {Provider} from 'react-redux';
import App from '../../App';

const store = configureStore();

const Setup = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

export default Setup;
