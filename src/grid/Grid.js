import {rotate, fillRandomItem, fill, fillLeft} from './GridUtils';

const {SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN} = {
  SWIPE_LEFT: 0, SWIPE_RIGHT: 1, SWIPE_UP: 2, SWIPE_DOWN: 4,
};

const getMeta = (initialIdx, reversed, length, merged) => {
  const indexAfter = reversed ? 3 - length : length;
  const newIdx = reversed ? indexAfter + 1 : indexAfter - 1;
  return {initialIdx, idx: merged ? newIdx : indexAfter};
};

let metadata = [];
let mergedOnPrevStep = false;

const merge = (row, reversed = true) => {
  const merged = [];
  let rowMetadata = [];
  row.map((item, idx) => {
    const initialIdx = reversed ? 3 - idx : idx;
    if (item) {
      rowMetadata.push(
        getMeta(initialIdx, reversed, merged.length, mergedOnPrevStep),
      );
      const nextObj = row.find((val, index) => val && index > idx);

      if (nextObj && item === nextObj && !mergedOnPrevStep) {
        mergedOnPrevStep = true;
        return merged.push(item * 2);
      }
      if (mergedOnPrevStep === true) {
        mergedOnPrevStep = false;
        return;
      }
      mergedOnPrevStep = false;
      return merged.push(item);
    }
  });
  metadata.push(rowMetadata);
  rowMetadata = [];
  return merged;
};

const leftAction = row => fill(merge(row, false));
const rightAction = row => fillLeft(merge(row.reverse()).reverse());

const moveGrid = (current, direction) => {
  metadata = [];
  let moved;
  switch (direction) {
    case SWIPE_LEFT:
      moved = current.map(row => leftAction(row));
      break;
    case SWIPE_RIGHT:
      moved = current.map(row => rightAction([...row]));
      break;
    case SWIPE_UP:
      moved = rotate(current).map(row => leftAction(row));
      moved = rotate(moved);

      break;
    case SWIPE_DOWN:
      moved = rotate(current).map(row => rightAction([...row]));
      moved = rotate(moved);
      break;
  }
  if (JSON.stringify(current) == JSON.stringify(moved)) {
    return {grid: moved, metadata};
  }
  const {grid, newValueCoordinates} = fillRandomItem(moved);
  return {grid, metadata, newValueCoordinates};
};

export default moveGrid;
