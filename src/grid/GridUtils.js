export const rotate = grid => {
  const rotated = [[], [], [], []];
  for (let i = 0; i < 4; i++) {
    grid.map(row => rotated[i].push(row[i]));
  }
  return rotated;
};

const findEmptyCells = grid => {
  const emptyCells = [];
  [...grid].map((row, rowIdx) => {
    row.map((item, idx) => {
      if (!item) {
        emptyCells.push([rowIdx, idx]);
      }
    });
  });
  return emptyCells;
};

export const fillRandomItem = grid => {
  const emptyCells = findEmptyCells(grid);
  const arrLen = emptyCells.length;
  if (arrLen === 0) {
    return grid;
  }
  const randomIndex = Math.floor(Math.random() * arrLen);
  const [x, y] = emptyCells[randomIndex];
  grid[x][y] = randomNum();
  return {grid, newValueCoordinates: [x, y]};
};

const randomNum = () => {
  const val = Math.floor(Math.random() * 10);
  if (val === 0) return 4;
  return 2;
};

export const fill = arr => arr.concat(new Array(4 - arr.length).fill(null));
export const fillLeft = arr => new Array(4 - arr.length).fill(null).concat(arr);

export const intitialGrid = [
  [2, null, null, 2],
  [2, null, 2, null],
  [null, null, null, 2],
  [4, 8, 4, 4],
];
