import React from 'react';
import {View, StyleSheet} from 'react-native';
import StorybookRoot from '../../storybook/index';
const Storybook = () => {
  return (
    <View style={styles.container}>
      <StorybookRoot />
    </View>
  );
};

export default Storybook;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
