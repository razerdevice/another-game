import React, {Component} from 'react';
import {View, StyleSheet, Dimensions, Animated, Easing, Text} from 'react-native';
import {connect} from 'react-redux';
import {updateGrid} from '../actions/actionCreators';
import {palette} from '../theme/variables';
import MyText from '../components/MyText';
import Toast from '../components/Toast';
import moveGrid from '../grid/Grid';
// import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import { cloneDeep } from 'lodash';

// const {SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
const {width} = Dimensions.get('window');
const TILE_WIDTH = (width * 0.5 - 20) / 4;
const {SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN} = {
  SWIPE_LEFT: 0, SWIPE_RIGHT: 1, SWIPE_UP: 2, SWIPE_DOWN: 4,
};

const KEY_CODES = {
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
};

class Game extends Component {

  state = {
    metadata: null,
    horizontal: null,
    newValueCoordinates: null,
    // Animation related values,
    move: new Animated.Value(0),
    opacity: new Animated.Value(0),
  };

  componentDidMount() {
    this.toastRef.animateToast('Swipe on grid to move tiles');

    // this.interval = setInterval(() => {
    //   const arr = [SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN]
    //   this.move(arr[Math.floor(Math.random() * 3)]);
    // }, 1000);

    document.addEventListener("keydown", this._handleKeyDown);
  }

  _handleKeyDown = (event) => {
    switch( event.keyCode ) {
      case KEY_CODES.LEFT:
        this.move(SWIPE_LEFT)
        break;
      case KEY_CODES.UP:
        this.move(SWIPE_UP)
        break;
      case KEY_CODES.DOWN:
        this.move(SWIPE_DOWN)
        break;
      case KEY_CODES.RIGHT:
        this.move(SWIPE_RIGHT)
        break;
    }
  };

  componentWillUnmount() {
    document.removeEventListener("keydown", this._handleKeyDown);

    if (this.interval) {
      clearInterval(this.interval);

    }
  }

  move = (direction) => {
    this.toastRef.animateToast(direction);

    const {grid, metadata, newValueCoordinates} = moveGrid(
      cloneDeep(this.props.grid),
      direction,
    );

    const horizontal = direction === SWIPE_LEFT || direction === SWIPE_RIGHT;
    this.setState({metadata, horizontal}, () => {
      this.moveTile([...grid], newValueCoordinates);
    });
  };


  moveTile = (grid, newValueCoordinates) => {
    const {updateGrid} = this.props;
    const {move} = this.state;

    move.setValue(0);

    Animated.timing(move, {
      toValue: 1,
      duration: 100,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      this.setState({metadata: null, newValueCoordinates});
      updateGrid(grid);
      this.animateNewValue();
    });
  };

  animateNewValue = () => {
    const {opacity} = this.state;
    opacity.setValue(0);
    Animated.timing(opacity, {toValue: 1, duration: 250, useNativeDriver: true}).start();
  };

  getPosition = (index, rowIdx) => {
    const {horizontal, metadata} = this.state;
    const staticLeft = index * TILE_WIDTH;
    const staticTop = rowIdx * TILE_WIDTH;

    if (!metadata) {
      return {top: staticTop, left: staticLeft};
    }

    const transitionCoordinate = horizontal ? this.interpolate(index, rowIdx) : this.interpolate(rowIdx, index);

    const left = horizontal ? transitionCoordinate : staticLeft;
    const top = horizontal ? staticTop : transitionCoordinate;
    return {top, left};
  };

  interpolate = (xIdx, yIdx) => {
    const {metadata, move} = this.state;
    const staticIndex = xIdx * TILE_WIDTH;

    // Find tile in metadata array and checks that tile index have changed
    const newIndex = metadata
      && metadata[yIdx]
        .find(({initialIdx, idx}) => initialIdx === xIdx && idx !== xIdx);

    if (!newIndex) return staticIndex;

    return move.interpolate({
      inputRange: [0, 1],
      outputRange: [TILE_WIDTH * xIdx, TILE_WIDTH * newIndex.idx],
    });
  };

  onSwipe = (gestureName, gestureState) => {
    return gestureName && this.move(gestureName);
  };

  renderRow = (row, rowIdx) => {
    return row.map((value, index) => {
      if (!value) return;

      const tileStyle = this.getTileStyle(index, rowIdx);

      return (
        <Animated.View style={tileStyle} key={`c-${index}r-${rowIdx}`}>
          <View
            style={[styles.tileBody, {backgroundColor: palette[value]}]}
          >
            <MyText style={styles.tileNumber}>{value}</MyText>
          </View>
        </Animated.View>
      );
    });
  };

  getTileStyle = (index, rowIdx) => {
    const {left, top} = this.getPosition(index, rowIdx);

    const {opacity} = this.state;

    let tileStyle = [styles.tile, {
      // left,
      // top,
      transform: [{
        translateX: left,
      }, {
        translateY: top,
      }]
    }];

    const {newValueCoordinates} = this.state;
    if (newValueCoordinates) {
      const [x, y] = newValueCoordinates;
      if (x === rowIdx && y === index) {
        tileStyle = [...tileStyle, {opacity}];
      }
    }

    return tileStyle;
  };

  render() {
    console.log("Render");

    const config = {
      velocityThreshold: 0.1,
      directionalOffsetThreshold: 150,
      gestureIsClickThreshold: 10,
    };
    return (
      <View style={styles.container}>
        <View
          style={styles.field}
          // onSwipe={(direction, state) => this.onSwipe(direction, state)}
          // config={config}
        >
          <View style={styles.fieldInner}>
            {this.props.grid.map((row, rowIdx) => this.renderRow(row, rowIdx))}
          </View>
        </View>
        <Toast ref={el => (this.toastRef = el)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: palette.sand,
    height: "100%",
  },
  field: {
    width: width * 0.5,
    height: width * 0.5,
    borderRadius: 15,
    backgroundColor: palette.clay,
    padding: 10,
  },
  fieldInner: {
    flex: 1,
  },
  tile: {
    width: TILE_WIDTH,
    aspectRatio: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
  },
  tileBody: {
    width: TILE_WIDTH * 0.9,
    aspectRatio: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tileNumber: {
    fontSize: 34,
    color: palette.tileNumber,
    fontWeight: '700',
  },
  appName: {
    color: palette.aqua,
    fontSize: 20,
    fontWeight: '400',
    padding: 20,
  },
  controlText: {
    fontSize: 20,
    marginVertical: 10,
    color: palette.tileNumber,
    textAlign: 'center',
  },
});

const mapStateToProps = state => {
  const {grid} = state.mainReducer;
  return {grid};
};

const mapDispatchToProps = dispatch => ({
  updateGrid: data => dispatch(updateGrid(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Game);
