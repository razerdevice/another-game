import React from 'react';
import {SafeAreaView, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {palette} from '../theme/variables';
import MyText from '../components/MyText';
import {useSelector, useDispatch} from 'react-redux';
import {testAction} from '../actions/actionCreators';

const Home = ({}) => {
  const appName = useSelector(state => state.mainReducer.appName);
  const dispatch = useDispatch();

  const handlePress = () => {
    const newName = appName + 't';
    dispatch(testAction(newName));
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <TouchableOpacity onPress={handlePress}>
        <MyText style={styles.appName}>{appName}</MyText>
      </TouchableOpacity>
      <View style={styles.container}>
        <TouchableOpacity onPress={Actions.Storybook}>
          <MyText style={styles.text}>Storybook</MyText>
        </TouchableOpacity>
        <TouchableOpacity onPress={Actions.Game}>
          <MyText style={styles.text}>Play 2048</MyText>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    paddingBottom: 10,
    fontSize: 18,
    color: palette.darkGrey,
  },
  appName: {
    color: palette.aqua,
    fontSize: 20,
    fontWeight: '400',
    padding: 20,
  },
});
