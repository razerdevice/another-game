const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: {
        main: './index.web.js',
    },
    output: {
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        [
                            "@babel/preset-env",
                            {
                                "loose": false,
                                "targets": { "esmodules": true },
                                "bugfixes": true
                            }
                        ],
                        "@babel/preset-react"
                    ],
                    plugins: [
                        ["@babel/plugin-proposal-private-property-in-object", { "loose": true }],
                        ["@babel/plugin-proposal-class-properties", { "loose": true }],
                        ["@babel/plugin-proposal-private-methods", { "loose": true }]
                    ],
                },
            },
        ],
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./index.html",
            filename: "./index.html"
        })
    ],
    resolve: {
        alias: {
            'react-native': 'react-native-web',
        },
    },
};